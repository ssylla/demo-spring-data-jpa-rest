package org.ss.demospring.dal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.ss.demospring.bo.Address;
import org.ss.demospring.bo.Contact;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IContactRepositoryTest {
	
	@Autowired
	private IContactRepository dao;
	
	@Test
	void findByName() {
	}
	
	@Test
	void findByEmail() {
		
		Contact contact = dao.save( new Contact("Antoine le chieur!!", "ato@ss.org") );
		assertEquals( contact.getEmail(), dao.findByEmail( "ato@ss.org" ).getEmail() );
	}
	
	@Test
	void findByNameOrEmail() {
	}
}