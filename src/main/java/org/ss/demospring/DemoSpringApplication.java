package org.ss.demospring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.ss.demospring.bo.Contact;
import org.ss.demospring.dal.IContactRepository;

@SpringBootApplication
public class DemoSpringApplication  {
	
	@Value( "${guy:la classe}" )
	private String name;
	
	@Autowired
	private IContactRepository dao;
	
	@Configuration
	static class SecurityConfig extends WebSecurityConfigurerAdapter {
		@Override
		protected void configure( AuthenticationManagerBuilder auth ) throws Exception {
			
			PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
			auth.inMemoryAuthentication()
				.withUser( "antoine" ).password( encoder.encode( "paul" ) ).roles( "USER" ).and()
				.withUser( "sega" ).password( encoder.encode( "sylla" ) ).roles( "USER", "SUPER_USER" );
		}
	}
	
	public static void main( String[] args ) {
		SpringApplication.run( DemoSpringApplication.class, args );
	}
	
	/*@Override
	public void run( String... args ) throws Exception {
		System.out.println( "Hello "+name+" !" );
		Contact contact = new Contact("Séga S", "ssy@ss.org");
		dao.save( contact );
		System.out.println(contact);
	}*/
}
