package org.ss.demospring.dal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.ss.demospring.bo.Contact;

import java.util.Collection;

@RepositoryRestResource(path = "contacts-list", collectionResourceRel = "contacts-data")
public interface IContactRepository extends CrudRepository<Contact, Long> {
	@RestResource(path = "name")
	Collection<Contact> findByName(@Param( "id" ) String name );
	Contact findByEmail(String email);
	Collection<Contact> findByNameOrEmail(String name, String email);
}
