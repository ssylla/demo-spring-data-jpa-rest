package org.ss.demospring.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Contact implements Serializable {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	@JsonIgnore//Pour l'export de cette donnée dans le REST
	@Column(unique = true)
	private String email;
	@JsonManagedReference//Pour éviter les chargements recursifs infinis dans le REST
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id")
	private Address address;
	
	public Contact() {}
	
	public Contact( String name, String email ) {
		this.name = name;
		this.email = email;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress( Address address ) {
		if ( this.address != null ) {
			this.address.getContacts().remove( this );
		}
		this.address = address;
		if ( this.address != null ) {
			this.address.getContacts().add( this );
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Contact{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
