package org.ss.demospring.bo;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Address implements Serializable {
	
	@Id
	@GeneratedValue
	private Long id;
	private String street;
	private String zipCode;
	private String city;
	
	@JsonBackReference//Pour éviter les chargements recursifs infinis dans le REST
	@OneToMany()
	private Set<Contact> contacts = new HashSet<>();
	
	public Address() {}
	
	public Address( String street, String zipCode, String city ) {
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet( String street ) {
		this.street = street;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
	public void setZipCode( String zipCode ) {
		this.zipCode = zipCode;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity( String city ) {
		this.city = city;
	}
	
	public void addContact(Contact contact) {
		if ( contact != null ) {
			contact.setAddress( this );
		}
	}
	public Set<Contact> getContacts() {
		return contacts;
	}
	
	public void setContacts( Set<Contact> contacts ) {
		this.contacts = contacts;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Address{" );
		sb.append( "id=" ).append( id );
		sb.append( ", street='" ).append( street ).append( '\'' );
		sb.append( ", zipCode='" ).append( zipCode ).append( '\'' );
		sb.append( ", city='" ).append( city ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
